using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.TechnologyService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace MOD.TechnologyService.Tests
{
    public class TechnologyControllerTests
    {
        private Mock<ITechnologyService> _technologyService;
        private Mock<ITechnologyRepository> _technologyRepository;
        private TechnologyController _controller;
        public TechnologyControllerTests()
        {
            _technologyRepository = new Mock<ITechnologyRepository>();
            _technologyService = new Mock<ITechnologyService>();
            _controller = new TechnologyController(_technologyService.Object);
        }

        [Fact]
        public async Task AddTechnology()
        {
            var techRequest = new TechnologyRequest { Technology = new Technology { } };
            _technologyService.Setup(x => x.Add(It.IsAny<Technology>())).Returns(Task.FromResult(new Technology()));
            var result = await _controller.AddTechnology(techRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(techRequest.Technology, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task GetAllTechnologiesTest()
        {
            var lst = new List<Technology>();
            _technologyService.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllTechnologies();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Technology>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetTechnologyByIdTest()
        {
            var lst = new List<Technology>();
            _technologyService.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Technology { Id = 1 }));
            var result = await _controller.GetTechnologyById(1);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, ((Technology)((OkObjectResult)result).Value).Id);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var technology1 = new Technology();
            var technologyRequest = new TechnologyRequest { Technology = new Technology { } };
            _technologyService.Setup(x => x.Update(It.IsAny<Technology>(), It.IsAny<Technology>())).Returns(Task.FromResult(new Technology()));
            var result = await _controller.UpdateTechnology(technologyRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(technologyRequest.Technology, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task DeleteTest()
        {
            var technologyRequest = new TechnologyRequest { Technology = new Technology { } };
            _technologyService.Setup(x => x.Delete(It.IsAny<Technology>())).Returns(Task.FromResult(new Technology()));
            var result = await _controller.DeleteTechnology(technologyRequest);
            Assert.IsType<OkObjectResult>(result);
        }
    }
}