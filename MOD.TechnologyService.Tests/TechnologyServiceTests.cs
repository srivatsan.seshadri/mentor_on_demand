﻿using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using MOD.Services.Implementations;
using System.Collections.Generic;
using System.Linq;

namespace MOD.TechnologyService.Tests
{
    public class TechnologyServiceTests
    {
        private readonly MOD.Services.Implementations.TechnologyService _techService;
        private readonly Mock<ITechnologyRepository> _techRepository;
        public TechnologyServiceTests()
        {
            _techRepository = new Mock<ITechnologyRepository>();
            _techService = new Services.Implementations.TechnologyService(_techRepository.Object);
        }

        [Fact]
        public async Task GetAllTechnologiesTest()
        {
            var lst = new List<Technology>();
            _techRepository.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _techService.GetAll();
            Assert.IsAssignableFrom<IEnumerable<Technology>>(result);
            Assert.Empty(((List<Technology>)result));
        }

        [Fact]
        public async Task GetTechnologyByIdTest()
        {
            var lst = new List<Technology>();
            _techRepository.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Technology { Id = 1 }));
            var result = await _techService.Get(1);
            Assert.IsType<Technology>(result);
            Assert.Equal(1, ((Technology)result).Id);
        }
    }
}
