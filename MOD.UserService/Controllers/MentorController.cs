﻿using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MOD.UserService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MentorController : ControllerBase
    {
        private readonly IMentorService _mentorService;
        public MentorController(IMentorService mentorService)
        {
            _mentorService = mentorService;
        }

        [HttpGet]
        [Route("GetAllMentors")]
        public async Task<IActionResult> GetAllMentors()
        {
            try
            {
                var users = await _mentorService.GetAll();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetMentorById/{id}")]
        public async Task<IActionResult> GetMentorById(int id)
        {
            try
            {
                var users = await _mentorService.Get(id);
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddMentor")]
        public async Task<IActionResult> AddMentor(MentorRequest request)
        {
            try
            {
                request.Mentor.RegCode = Guid.NewGuid().ToString();
                request.Mentor.RegDateTime = DateTime.Now;
                request.Mentor.Active = true;
                await _mentorService.Add(request.Mentor);
                return Ok(request.Mentor);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdateMentor")]
        public async Task<IActionResult> UpdateMentor(MentorRequest request)
        {
            try
            {
                var mentor = await _mentorService.Get(request.Mentor.Id);
                await _mentorService.Update(mentor, request.Mentor);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("DeleteMentor")]
        public async Task<IActionResult> DeleteMentor(MentorRequest request)
        {
            try
            {
                await _mentorService.Delete(request.Mentor);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}