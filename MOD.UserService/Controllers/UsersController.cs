﻿using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MOD.UserService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var users = await _userService.GetAll();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetUserById/{id}")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                var users = await _userService.Get(id);
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddUser")]
        public async Task<IActionResult> AddUser(UserRequest request)
        {
            try
            {
                request.User.RegCode = Guid.NewGuid().ToString();
                request.User.RegDateTime = DateTime.Now;
                request.User.Active = true;
                await _userService.Add(request.User);
                request.User.MentorSkills = null;
                return Ok(request.User);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UserRequest request)
        {
            try
            {
                var user = await _userService.Get(request.User.Id);
                await _userService.Update(user, request.User);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("DeleteUser")]
        public async Task<IActionResult> DeleteUser(UserRequest request)
        {
            try
            {
                await _userService.Delete(request.User);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpGet]
        [Route("GetAllRoles")]
        public async Task<IActionResult> GetAllRoles()
        {
            try
            {
                var users = await _userService.GetAllRoles();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetMentorSkillsById/{id}")]
        public async Task<IActionResult> GetMentorSkillsById(int id)
        {
            try
            {
                var users = await _userService.GetUserMentorSkills(id);
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAllMentors")]
        public async Task<IActionResult> GetAllMentors()
        {
            try
            {
                var users = await _userService.GetAllMentors();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}