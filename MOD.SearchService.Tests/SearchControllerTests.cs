using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.SearchService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace MOD.SearchService.Tests
{
    public class SearchControllerTests
    {
        private Mock<ISearchService> _searchService;
        private Mock<ISearchRepository> _searchRepository;

        private Mock<IProposalService> _proposalService;
        private Mock<IProposalRepository> _proposalRepository;

        private SearchResultController _controller;
        public SearchControllerTests()
        {
            _searchRepository = new Mock<ISearchRepository>();
            _searchService = new Mock<ISearchService>();

            _proposalRepository = new Mock<IProposalRepository>();
            _proposalService = new Mock<IProposalService>();

            _controller = new SearchResultController(_searchService.Object, _proposalService.Object);
        }
        [Fact]
        public async Task SearchMentorsForTechnologies()
        {
            var lst = new List<SearchResult>();
            _searchService.Setup(x => x.SearchMentorsForTechnologies(It.IsAny<int>(), It.IsAny<DateTime?>(), It.IsAny<DateTime?>())).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.SearchMentorsForTechnologies(1,null,null);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task AddProposal()
        {
            var proposalRequest = new ProposalRequest { Proposal = new Proposal { } };
            _proposalService.Setup(x => x.Add(It.IsAny<Proposal>())).Returns(Task.FromResult(new Proposal()));
            var result = await _controller.AddProposal(proposalRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(proposalRequest.Proposal, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task GetAllProposalsTest()
        {
            var lst = new List<Proposal>();
            _proposalService.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllProposals();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Proposal>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetProposalByIdTest()
        {
            var lst = new List<Proposal>();
            _proposalService.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Proposal { Id = 1 }));
            var result = await _controller.GetProposalsById(1);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, ((Proposal)((OkObjectResult)result).Value).Id);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var payment1 = new Proposal();
            var proposalRequest = new ProposalRequest { Proposal = new Proposal { } };
            _proposalService.Setup(x => x.Update(It.IsAny<Proposal>(), It.IsAny<Proposal>())).Returns(Task.FromResult(new Proposal()));
            var result = await _controller.UpdateProposal(proposalRequest);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task DeleteTest()
        {
            var payment1 = new Proposal();
            var proposalRequest = new ProposalRequest { Proposal = new Proposal { } };
            _proposalService.Setup(x => x.Delete(It.IsAny<Proposal>())).Returns(Task.FromResult(new Proposal()));
            var result = await _controller.DeleteProposal(proposalRequest);
            Assert.IsType<OkResult>(result);
        }
    }
}
