﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Models
{
    public class SearchResult
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Rating { get; set; }
        public int YearsOfExperience { get; set; }
    }
}
