﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Models
{
    public class TrainingResponse
    {
        public int Id { get; set; }

        public string MentorName { get; set; }
        public int MentorId { get; set; }
        public string Skill { get; set; }
        public int SkillId { get; set; }
        public string Status { get; set; }
        public int Progress { get; set; }
        public int Rating { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal AmountReceived { get; set; }
        public Decimal? Fees { get; set; }
    }
}
