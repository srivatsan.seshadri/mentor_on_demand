﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Models
{
    public class Error
    {
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
    }
}
