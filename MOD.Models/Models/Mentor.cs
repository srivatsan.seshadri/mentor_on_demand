﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Models
{
    public class Mentor:User
    {
        public string LinkedUrl { get; set; }
        public int YearsOfExperience { get; set; }
        public List<MentorCalendar> MentorCalendars { get; set; }
        public List<MentorSkill> MentorSkills { get; set; }
        public List<Payment> MentorPayments { get; set; }
        public List<Training> MentorTrainings { get; set; }
    }
}
