﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MOD.Models.Models
{
    public class Training
    {
        [Key()]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Id")]
        public User Mentor { get; set; }
        public int MentorId { get; set; }

        [ForeignKey("PaymentId")]
        public Payment Payment { get; set; }
        public int PaymentId { get; set; }

        public int UserId { get; set; }
        public Technology Skill { get; set; }
        public int SkillId { get; set; }
        public string Status { get; set; }
        public int Progress { get; set; }
        public int Rating { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal AmountReceived { get; set; }
        public Decimal? Fees { get; set; }
    }
}
