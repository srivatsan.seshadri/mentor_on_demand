﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Models
{
    public class PaymentResponse
    {
       public int Id { get; set; }
        public string MentorFirstName { get; set; }
        public string MentorLastName { get; set; }
        public int MentorId { get; set; }
        public string Training { get; set; }
        public int TrainingId { get; set; }
        public string TxnType { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Remarks { get; set; }
    }
}
