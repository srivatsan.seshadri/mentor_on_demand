﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MOD.Models.Models
{
    public class User
    {
        [Key()]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public DateTime RegDateTime { get; set; }
        public string RegCode { get; set; }
        public bool ForceResetPassword { get; set; }
        public bool Active { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        public int RoleId { get; set; }
        public DateTime LastLoginDate { get; set; }
        public List<MentorSkill> MentorSkills { get; set; }
        public string LinkedInUrl { get; set; }
        public int? YearsOfExperience { get; set; }
        public List<MentorCalendar> MentorCalendars { get; set; }
        public List<Payment> MentorPayments { get; set; }
        public List<Training> MentorTrainings { get; set; }
    }
}
