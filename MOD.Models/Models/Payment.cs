﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MOD.Models.Models
{
    public class Payment
    {
        [Key()]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("MentorId")]
        public User Mentor { get; set; }
        public int MentorId { get; set; }
        [ForeignKey("TrainingId")]
        public Training Training { get; set; }
        public int TrainingId { get; set; }
        public string TxnType { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Remarks { get; set; }
    }
}
