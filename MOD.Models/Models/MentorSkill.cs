﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MOD.Models.Models
{
    public class MentorSkill
    {
        [Key()]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("MentorId")]
        public User Mentor { get; set; }
        public int MentorId { get; set; }
        public int SkillId { get; set; }
        public int SelfRating { get; set; }
        public int YearsOfExperience { get; set; }
        public int TrainingsDelivered { get; set; }
        public string FacilitiesOffered { get; set; }
        public string SkillName { get; set; }
    }
}
