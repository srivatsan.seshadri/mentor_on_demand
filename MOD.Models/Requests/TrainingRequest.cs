﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Requests
{
    public class TrainingRequest
    {
        public Training Training { get; set; }
    }
}
