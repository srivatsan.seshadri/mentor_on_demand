﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Requests
{
    public class UserRequest
    {
        public User User { get; set; }
        public List<MentorSkill> MentorSkills { get; set; }
    }
}
