﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Requests
{
    public class TechnologyRequest
    {
        public Technology Technology { get; set; }
    }
}
