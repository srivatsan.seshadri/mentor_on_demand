﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Requests
{
    public class MentorRequest
    {
        public User Mentor { get; set; }
    }
}
