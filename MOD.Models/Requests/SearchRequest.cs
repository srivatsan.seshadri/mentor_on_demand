﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Models.Requests
{
    public class SearchRequest
    {
        public int TechnologyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
