﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;

namespace MOD.SearchService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchResultController : ControllerBase
    {
        private readonly ISearchService _searchService;
        private readonly IProposalService _proposalService;
        public SearchResultController(ISearchService searchService, IProposalService proposalService)
        {
            _searchService = searchService;
            _proposalService = proposalService;
        }

        [HttpGet]
        [Route("SearchMentorsForTechnologies/{id}/{fromDate}/{toDate}")]
        public async Task<IActionResult> SearchMentorsForTechnologies(int id, DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var searchResult = await _searchService.SearchMentorsForTechnologies(id, fromDate, toDate);
                return Ok(searchResult);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAllProposals")]
        public async Task<IActionResult> GetAllProposals()
        {
            try
            {
                var proposals = await _proposalService.GetAll();
                return Ok(proposals);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetProposalsById/{id}")]
        public async Task<IActionResult> GetProposalsById(int id)
        {
            try
            {
                var proposal = await _proposalService.Get(id);
                return Ok(proposal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddProposal")]
        public async Task<IActionResult> AddProposal(ProposalRequest request)
        {
            try
            {
                await _proposalService.Add(request.Proposal);
                return Ok(request.Proposal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdateProposal")]
        public async Task<IActionResult> UpdateProposal(ProposalRequest request)
        {
            try
            {
                var proposal = await _proposalService.Get(request.Proposal.Id);
                await _proposalService.Update(proposal, request.Proposal);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("DeleteProposal")]
        public async Task<IActionResult> DeleteProposal(ProposalRequest request)
        {
            try
            {
                await _proposalService.Delete(request.Proposal);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}