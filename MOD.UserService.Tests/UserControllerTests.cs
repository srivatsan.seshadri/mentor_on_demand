using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.UserService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
namespace MOD.UserService.Tests
{
    public class UserControllerTests
    {
        private Mock<IUserService> _userService;
        private Mock<IUserRepository> _userRepository;
        private UsersController _controller;
        public UserControllerTests()
        {
            _userRepository = new Mock<IUserRepository>();
            _userService = new Mock<IUserService>();
            _controller = new UsersController(_userService.Object);
        }

        [Fact]
        public async Task AddUser()
        {
            var techRequest = new UserRequest { User = new User { } };
            _userService.Setup(x => x.Add(It.IsAny<User>())).Returns(Task.FromResult(new User()));
            var result = await _controller.AddUser(techRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(techRequest.User, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task GetAllUsersTest()
        {
            var lst = new List<User>();
            _userService.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllUsers();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<User>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetAllMentorsTest()
        {
            var lst = new List<User>();
            _userService.Setup(x => x.GetAllMentors()).Returns(Task.FromResult(lst));
            var result = await _controller.GetAllMentors();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Models.Models.User>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetAllRolesTest()
        {
            var lst = new List<Role>();
            _userService.Setup(x => x.GetAllRoles()).Returns(Task.FromResult(lst));
            var result = await _controller.GetAllRoles();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Role>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetUserByIdTest()
        {
            var lst = new List<User>();
            _userService.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new User { Id = 1 }));
            var result = await _controller.GetUserById(1);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, ((User)((OkObjectResult)result).Value).Id);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var User = new User();
            var UserRequest = new UserRequest { User = new User { } };
            _userService.Setup(x => x.Update(It.IsAny<User>(), It.IsAny<User>())).Returns(Task.FromResult(new User()));
            var result = await _controller.UpdateUser(UserRequest);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task DeleteTest()
        {
            var UserRequest = new UserRequest { User = new User { } };
            _userService.Setup(x => x.Delete(It.IsAny<User>())).Returns(Task.FromResult(new User()));
            var result = await _controller.DeleteUser(UserRequest);
            Assert.IsType<OkResult>(result);
        }
    }
}
