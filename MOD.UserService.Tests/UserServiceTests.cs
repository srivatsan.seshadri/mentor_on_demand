﻿using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using MOD.Services.Implementations;
using System.Collections.Generic;
using System.Linq;


namespace MOD.TrainingService.Tests
{
    public class UserServiceTests
    {
        private readonly MOD.Services.Implementations.UserService _userService;
        private Mock<IUserRepository> _userRepository;
        public UserServiceTests()
        {
            _userRepository = new Mock<IUserRepository>();
            _userService = new Services.Implementations.UserService(_userRepository.Object);
        }

        [Fact]
        public async Task GetAllUsersTest()
        {
            var lst = new List<User>();
            _userRepository.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _userService.GetAll();
            Assert.IsType<List<User>>(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetAllMentorsTest()
        {
            var lst = new List<User>();
            _userRepository.Setup(x => x.GetAllMentors()).Returns(Task.FromResult(lst));
            var result = await _userService.GetAllMentors();
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetAllRolesTest()
        {
            var lst = new List<Role>();
            _userRepository.Setup(x => x.GetAllRoles()).Returns(Task.FromResult(lst));
            var result = await _userService.GetAllRoles();
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetUserByIdTest()
        {
            var lst = new List<User>();
            _userRepository.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new User { Id = 1 }));
            var result = await _userService.Get(1);
            Assert.IsType<User>(result);
            Assert.Equal(1, result.Id);
        }
    }
}
