﻿using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MOD.TrainingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingsController : ControllerBase
    {
        private readonly ITrainingService _trainingService;
        public TrainingsController(ITrainingService trainingService)
        {
            _trainingService = trainingService;
        }

        [HttpGet]
        [Route("GetAllTrainings")]
        public async Task<IActionResult> GetAllTrainings()
        {
            try
            {
                var trainings = await _trainingService.GetAll();
                return Ok(trainings);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAllTrainingsForList")]
        public async Task<IActionResult> GetAllTrainingsForList()
        {
            try
            {
                var trainings = await _trainingService.GetAllTrainingsForList();
                return Ok(trainings);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetTrainingById/{id}")]
        public async Task<IActionResult> GetTrainingById(int id)
        {
            try
            {
                var trainings = await _trainingService.Get(id);
                return Ok(trainings);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddTraining")]
        public async Task<IActionResult> AddTraining(TrainingRequest request)
        {
            try
            {
                await _trainingService.Add(request.Training);
                return Ok(request.Training);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdateTraining")]
        public async Task<IActionResult> UpdateTraining(TrainingRequest request)
        {
            try
            {
                var training = await _trainingService.Get(request.Training.Id);
                await _trainingService.Update(training, request.Training);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("DeleteTraining")]
        public async Task<IActionResult> DeleteTraining(TrainingRequest request)
        {
            try
            {
                await _trainingService.Delete(request.Training);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}