﻿using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using MOD.Services.Implementations;
using System.Collections.Generic;
using System.Linq;

namespace MOD.PaymentService.Tests
{
    public class PaymentServiceTests
    {
        private readonly MOD.Services.Implementations.PaymentService _paymentService;
        private readonly Mock<IPaymentRepository> _paymentRepository;
        public PaymentServiceTests()
        {
            _paymentRepository = new Mock<IPaymentRepository>();
            _paymentService = new Services.Implementations.PaymentService(_paymentRepository.Object);
        }

        [Fact]
        public async Task GetAllPaymentsTest()
        {
            var lst = new List<Payment>();
            _paymentRepository.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _paymentService.GetAll();
            Assert.IsAssignableFrom<IEnumerable<Payment>>(result);
            Assert.Empty(((List<Payment>)result));
        }

        [Fact]
        public async Task GetPaymentByIdTest()
        {
            var lst = new List<Payment>();
            _paymentRepository.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Payment { Id = 1 }));
            var result = await _paymentService.Get(1);
            Assert.IsType<Payment>(result);
            Assert.Equal(1, ((Payment)result).Id);
        }

        [Fact]
        public async Task GetAllPaymentsForListTest()
        {
            var lst = new List<PaymentResponse>();
            _paymentRepository.Setup(x => x.GetAllPaymentsForList()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _paymentService.GetAllPaymentsForList();
            Assert.IsAssignableFrom<IEnumerable<PaymentResponse>>(result);
            Assert.Empty(((List<PaymentResponse>)result));
        }
    }
}
