using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.PaymentService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace MOD.PaymentService.Tests
{
    public class PaymentControllerTests
    {
        private Mock<IPaymentService> _paymentService;
        private Mock<IPaymentRepository> _paymentRepository;
        private PaymentController _controller;
        public PaymentControllerTests()
        {
            _paymentRepository = new Mock<IPaymentRepository>();
            _paymentService = new Mock<IPaymentService>();
            _controller = new PaymentController(_paymentService.Object);
        }

        [Fact]
        public async Task AddPayment()
        {
            var paymentRequest = new PaymentRequest { Payment = new Payment { } };
            _paymentService.Setup(x => x.Add(It.IsAny<Payment>())).Returns(Task.FromResult(new Payment()));
            var result = await _controller.AddPayment(paymentRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(paymentRequest.Payment, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task GetAllPaymentsTest()
        {
            var lst = new List<Payment>();
            _paymentService.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllPayments();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Payment>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetPaymentByIdTest()
        {
            var lst = new List<Payment>();
            _paymentService.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Payment { Id = 1 }));
            var result = await _controller.GetPaymentById(1);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, ((Payment)((OkObjectResult)result).Value).Id);
        }

        [Fact]
        public async Task GetAllPaymentsForListTest()
        {
            var lst = new List<PaymentResponse>();
            _paymentService.Setup(x => x.GetAllPaymentsForList()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllPaymentsForList();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<PaymentResponse>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task UpdateTest()
        {
            var payment1 = new Payment();
            var paymentRequest = new PaymentRequest { Payment = new Payment { } };
            var lst = new List<PaymentResponse>();
            _paymentService.Setup(x => x.Update(It.IsAny<Payment>(),It.IsAny<Payment>())).Returns(Task.FromResult(new Payment()));
            var result = await _controller.UpdatePayment(paymentRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(paymentRequest.Payment, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task DeleteTest()
        {
            var payment1 = new Payment();
            var paymentRequest = new PaymentRequest { Payment = new Payment { } };
            var lst = new List<PaymentResponse>();
            _paymentService.Setup(x => x.Delete(It.IsAny<Payment>())).Returns(Task.FromResult(new Payment()));
            var result = await _controller.DeletePayment(paymentRequest);
            Assert.IsType<OkResult>(result);
        }
    }
}
