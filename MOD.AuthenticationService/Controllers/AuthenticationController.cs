﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;

namespace MOD.AuthenticationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [Route(nameof(ValidateUser))]
        public async Task<IActionResult> ValidateUser(AuthenticationRequest request)
        {
            try
            {
                var user = await _authenticationService.ValidateUser(request.UserName, request.Password);
                if (user != null)
                {
                    await _authenticationService.UpdateLastLogin(request.UserName);
                    return Ok(user);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}