﻿using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using MOD.Services.Implementations;
using System.Collections.Generic;
using System.Linq;


namespace MOD.TrainingService.Tests
{
    public class TrainingServiceTests
    {
        private readonly MOD.Services.Implementations.TrainingService _trainingService;
        private readonly Mock<ITrainingRepository> _trainingRepository;
        public TrainingServiceTests()
        {
            _trainingRepository = new Mock<ITrainingRepository>();
            _trainingService = new Services.Implementations.TrainingService(_trainingRepository.Object);
        }

        [Fact]
        public async Task GetAllTrainingsTest()
        {
            var lst = new List<Training>();
            _trainingRepository.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _trainingService.GetAll();
            Assert.IsAssignableFrom<IEnumerable<Training>>(result);
            Assert.Empty(((List<Training>)result));
        }

        [Fact]
        public async Task GetAllTrainingResponsesTest()
        {
            var lst = new List<Models.Models.TrainingResponse>();
            _trainingRepository.Setup(x => x.GetAllTrainingsForList()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _trainingService.GetAllTrainingsForList();
            Assert.IsAssignableFrom<IEnumerable<Models.Models.TrainingResponse>>(result);
            Assert.Empty(((List<Models.Models.TrainingResponse>)result));
        }


        [Fact]
        public async Task GetTrainingByIdTest()
        {
            var lst = new List<Training>();
            _trainingRepository.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Training { Id = 1 }));
            var result = await _trainingService.Get(1);
            Assert.IsType<Training>(result);
            Assert.Equal(1, ((Training)result).Id);
        }
    }
}
