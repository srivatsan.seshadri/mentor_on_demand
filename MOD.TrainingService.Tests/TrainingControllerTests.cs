using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.TrainingService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
namespace MOD.TrainingService.Tests
{
    public class TrainingControllerTests
    {
        private Mock<ITrainingService> _trainingService;
        private Mock<ITrainingRepository> _trainingRepository;
        private TrainingsController _controller;
        public TrainingControllerTests()
        {
            _trainingRepository = new Mock<ITrainingRepository>();
            _trainingService = new Mock<ITrainingService>();
            _controller = new TrainingsController(_trainingService.Object);
        }

        [Fact]
        public async Task AddTraining()
        {
            var techRequest = new TrainingRequest { Training = new Training { } };
            _trainingService.Setup(x => x.Add(It.IsAny<Training>())).Returns(Task.FromResult(new Training()));
            var result = await _controller.AddTraining(techRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(techRequest.Training, ((OkObjectResult)result).Value);
        }

        [Fact]
        public async Task GetAllTrainingsTest()
        {
            var lst = new List<Training>();
            _trainingService.Setup(x => x.GetAll()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllTrainings();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Training>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetAllTrainingsResponseTest()
        {
            var lst = new List<Models.Models.TrainingResponse>();
            _trainingService.Setup(x => x.GetAllTrainingsForList()).Returns(Task.FromResult(lst.AsEnumerable()));
            var result = await _controller.GetAllTrainingsForList();
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(((List<Models.Models.TrainingResponse>)((OkObjectResult)result).Value));
        }

        [Fact]
        public async Task GetTrainingByIdTest()
        {
            var lst = new List<Training>();
            _trainingService.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(new Training { Id = 1 }));
            var result = await _controller.GetTrainingById(1);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, ((Training)((OkObjectResult)result).Value).Id);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var training = new Training();
            var trainingRequest = new TrainingRequest { Training = new Training { } };
            _trainingService.Setup(x => x.Update(It.IsAny<Training>(), It.IsAny<Training>())).Returns(Task.FromResult(new Training()));
            var result = await _controller.UpdateTraining(trainingRequest);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task DeleteTest()
        {
            var trainingRequest = new TrainingRequest { Training = new Training { } };
            _trainingService.Setup(x => x.Delete(It.IsAny<Training>())).Returns(Task.FromResult(new Training()));
            var result = await _controller.DeleteTraining(trainingRequest);
            Assert.IsType<OkResult>(result);
        }
    }
}
