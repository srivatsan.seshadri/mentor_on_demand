﻿using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MOD.PaymentService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        [HttpGet]
        [Route("GetAllPayments")]
        public async Task<IActionResult> GetAllPayments()
        {
            try
            {
                var users = await _paymentService.GetAll();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAllPaymentsForList")]
        public async Task<IActionResult> GetAllPaymentsForList()
        {
            try
            {
                var users = await _paymentService.GetAllPaymentsForList();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetPaymentById/{id}")]
        public async Task<IActionResult> GetPaymentById(int id)
        {
            try
            {
                var users = await _paymentService.Get(id);
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddPayment")]
        public async Task<IActionResult> AddPayment(PaymentRequest request)
        {
            try
            {
                await _paymentService.Add(request.Payment);
                request.Payment.Mentor = null;
                request.Payment.Training = null;
                return Ok(request.Payment);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdatePayment")]
        public async Task<IActionResult> UpdatePayment(PaymentRequest request)
        {
            try
            {
                var user = await _paymentService.Get(request.Payment.Id);
                await _paymentService.Update(user, request.Payment);
                request.Payment.Mentor = null;
                request.Payment.Training = null;
                return Ok(request.Payment);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeletePayment")]
        public async Task<IActionResult> DeletePayment(PaymentRequest request)
        {
            try
            {
                await _paymentService.Delete(request.Payment);
                return Ok();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}