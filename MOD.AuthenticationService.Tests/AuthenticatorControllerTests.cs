using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.AuthenticationService.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace MOD.AuthenticationService.Tests
{
    public class AuthenticatorControllerTests
    {
        private Mock<IAuthenticationService> _authenticationService;
        private Mock<IAuthenticationRepository> _authenticationRepository;
        private AuthenticationController _controller;
        public AuthenticatorControllerTests()
        {
            _authenticationRepository = new Mock<IAuthenticationRepository>();
            _authenticationService = new Mock<IAuthenticationService>();
            _controller = new AuthenticationController(_authenticationService.Object);
        }

        [Fact]
        public async Task ValidateUserTestPass()
        {
            var authenticationRequest = new AuthenticationRequest { UserName = "admin", Password = "admin" };
            _authenticationService.Setup(svc => svc.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new User { Id = 1, UserName = "admin", Password = "admin" });
            _authenticationService.Setup(svc => svc.UpdateLastLogin(It.IsAny<string>()));
            var result = await _controller.ValidateUser(authenticationRequest);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(authenticationRequest.UserName, ((User)((OkObjectResult)result).Value).UserName);
        }

        [Fact]
        public async Task ValidateUserTestFail()
        {
            var authenticationRequest = new AuthenticationRequest();
            _authenticationService.Setup(svc => svc.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(It.IsAny<User>());
            var result = await _controller.ValidateUser(authenticationRequest);
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
