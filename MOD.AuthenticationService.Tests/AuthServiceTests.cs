﻿using System;
using Xunit;
using Moq;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;
using MOD.Models.Models;
using MOD.Models.Requests;
using MOD.AuthenticationService.Controllers;
using Microsoft.AspNetCore.Mvc;
using MOD.Services.Implementations;
namespace MOD.AuthenticationService.Tests
{
    public class AuthServiceTests
    {
        private readonly MOD.Services.Implementations.AuthenticationService _authenticationService;
        private readonly Mock<IAuthenticationRepository> _authenticationRepository;
        public AuthServiceTests()
        {
            _authenticationRepository = new Mock<IAuthenticationRepository>();
            _authenticationService = new Services.Implementations.AuthenticationService(_authenticationRepository.Object);
        }

        [Fact]
        public async Task ValidateUserTestPass()
        {
            _authenticationRepository.Setup(svc => svc.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new User { Id = 1, UserName = "admin", Password = "admin" });
            var result = await _authenticationService.ValidateUser("admin","admin");
            Assert.IsType<User>(result);
            Assert.Equal("admin", result.UserName);
        }

        [Fact]
        public async Task ValidateUserTestFailIfInputAreNull()
        {
            var authenticationRequest = new AuthenticationRequest();
            _authenticationRepository.Setup(svc => svc.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(It.IsAny<User>());
            var result = await _authenticationService.ValidateUser(null,null);
            Assert.Null(result);
        }
    }
}
