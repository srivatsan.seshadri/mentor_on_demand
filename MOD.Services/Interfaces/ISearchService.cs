﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface ISearchService
    {
        Task<IEnumerable<SearchResult>> SearchMentorsForTechnologies(int technology, DateTime? fromDate, DateTime? toDate);
    }
}
