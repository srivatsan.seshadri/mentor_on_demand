﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> Get(long id);
        Task Add(User entity);
        Task Update(User user, User entity);
        Task Delete(User user);
        Task<List<Role>> GetAllRoles();
        Task<List<MentorSkill>> GetUserMentorSkills(long userId);
        Task<List<User>> GetAllMentors();
    }
}
