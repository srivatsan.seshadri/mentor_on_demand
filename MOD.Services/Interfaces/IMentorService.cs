﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface IMentorService
    {
        Task<IEnumerable<Mentor>> GetAll();
        Task<Mentor> Get(long id);
        Task Add(Mentor entity);
        Task Update(Mentor mentor, Mentor entity);
        Task Delete(Mentor user);
    }
}
