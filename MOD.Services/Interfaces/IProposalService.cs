﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface IProposalService
    {
        Task<IEnumerable<Proposal>> GetAll();
        Task<Proposal> Get(long id);
        Task Add(Proposal entity);
        Task Update(Proposal payment, Proposal entity);
        Task Delete(Proposal payment);
    }
}
