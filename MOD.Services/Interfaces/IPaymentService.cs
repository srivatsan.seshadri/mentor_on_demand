﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<IEnumerable<Payment>> GetAll();
        Task<Payment> Get(long id);
        Task Add(Payment entity);
        Task Update(Payment payment, Payment entity);
        Task Delete(Payment payment);
        Task<IEnumerable<PaymentResponse>> GetAllPaymentsForList();
    }
}
