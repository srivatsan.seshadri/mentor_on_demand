﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
   public interface IAuthenticationService
    {
        Task<User> ValidateUser(string userName, string password);
        Task UpdateLastLogin(string userName);
    }
}
