﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Interfaces
{
    public interface ITechnologyService
    {
        Task<IEnumerable<Technology>> GetAll();
        Task<Technology> Get(long id);
        Task Add(Technology entity);
        Task Update(Technology technology, Technology entity);
        Task Delete(Technology technology);
    }
}
