﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class SearchService : ISearchService
    {
        private readonly ISearchRepository _searchRepository;
        public SearchService(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }
        public async Task<IEnumerable<SearchResult>> SearchMentorsForTechnologies(int technology, DateTime? fromDate, DateTime? toDate)
        {
            return await _searchRepository.SearchMentorsForTechnologies(technology, fromDate, toDate);
        }
    }
}
