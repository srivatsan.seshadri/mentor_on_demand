﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        public PaymentService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }
        public async Task Add(Payment entity)
        {
            await _paymentRepository.Add(entity);
        }

        public async Task Delete(Payment payment)
        {
            await _paymentRepository.Delete(payment);
        }

        public async Task<Payment> Get(long id)
        {
            return await _paymentRepository.Get(id);
        }

        public async Task<IEnumerable<Payment>> GetAll()
        {
            return await _paymentRepository.GetAll();
        }

        public async Task<IEnumerable<PaymentResponse>> GetAllPaymentsForList()
        {
            return await _paymentRepository.GetAllPaymentsForList();
        }

        public async Task Update(Payment payment, Payment entity)
        {
            await _paymentRepository.Update(payment,entity);
        }
    }
}
