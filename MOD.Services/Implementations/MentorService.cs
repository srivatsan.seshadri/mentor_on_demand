﻿using MOD.Models.Models;
using MOD.Repository.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class MentorService : IMentorService
    {
        private readonly IMentorRepository _mentorRepository;
        public MentorService(IMentorRepository mentorRepository)
        {
            _mentorRepository = mentorRepository;
        }
        public async Task Add(Mentor entity)
        {
            await _mentorRepository.Add(entity);
        }

        public async Task Delete(Mentor user)
        {
            await _mentorRepository.Delete(user);
        }

        public async Task<Mentor> Get(long id)
        {
            return await _mentorRepository.Get(id);
        }

        public async Task<IEnumerable<Mentor>> GetAll()
        {
            return await _mentorRepository.GetAll();
        }

        public async Task Update(Mentor mentor, Mentor entity)
        {
            await _mentorRepository.Update(mentor, entity);
        }
    }
}
