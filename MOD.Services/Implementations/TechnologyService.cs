﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class TechnologyService : ITechnologyService
    {
        private readonly ITechnologyRepository _technologyRepository;
        public TechnologyService(ITechnologyRepository technologyRepository)
        {
            _technologyRepository = technologyRepository;
        }
        public async Task Add(Technology entity)
        {
            await _technologyRepository.Add(entity);
        }

        public async Task Delete(Technology payment)
        {
            await _technologyRepository.Delete(payment);
        }

        public async Task<Technology> Get(long id)
        {
            return await _technologyRepository.Get(id);
        }

        public async Task<IEnumerable<Technology>> GetAll()
        {
            return await _technologyRepository.GetAll();
        }

        public async Task Update(Technology payment, Technology entity)
        {
            await _technologyRepository.Update(payment, entity);
        }
    }
}
