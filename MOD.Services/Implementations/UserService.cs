﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task Add(User entity)
        {
            await _userRepository.Add(entity);
        }

        public async Task Delete(User user)
        {
            await _userRepository.Delete(user);
        }

        public async Task<User> Get(long id)
        {
            return await _userRepository.Get(id);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userRepository.GetAll();
        }

        public async Task<List<User>> GetAllMentors()
        {
            return await _userRepository.GetAllMentors();
        }

        public async Task<List<Role>> GetAllRoles()
        {
            return await _userRepository.GetAllRoles();
        }

        public async Task<List<MentorSkill>> GetUserMentorSkills(long userId)
        {
            return await _userRepository.GetUserMentorSkills(userId);
        }

        public async Task Update(User user, User entity)
        {
            await _userRepository.Update(user, entity);
        }
    }
}
