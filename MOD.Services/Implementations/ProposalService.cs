﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class ProposalService : IProposalService
    {
        private readonly IProposalRepository _proposalRepository;
        public ProposalService(IProposalRepository proposalRepository)
        {
            _proposalRepository = proposalRepository;
        }
        public async Task Add(Proposal entity)
        {
            await _proposalRepository.Add(entity);
        }

        public async Task Delete(Proposal entity)
        {
            await _proposalRepository.Delete(entity);
        }

        public async Task<Proposal> Get(long id)
        {
           return await _proposalRepository.Get(id);
        }

        public async Task<IEnumerable<Proposal>> GetAll()
        {
            return await _proposalRepository.GetAll();
        }

        public async Task Update(Proposal proposal, Proposal entity)
        {
            await _proposalRepository.Update(proposal,entity);
        }
    }
}
