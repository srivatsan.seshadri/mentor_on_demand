﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class TrainingService : ITrainingService
    {
        private readonly ITrainingRepository _trainingRepository;
        public TrainingService(ITrainingRepository trainingRepository)
        {
            _trainingRepository = trainingRepository;
        }
        public async Task Add(Training entity)
        {
            await _trainingRepository.Add(entity);
        }

        public async Task Delete(Training training)
        {
            await _trainingRepository.Delete(training);
        }

        public async Task<Training> Get(long id)
        {
            return await _trainingRepository.Get(id);
        }

        public async Task<IEnumerable<Training>> GetAll()
        {
            return await _trainingRepository.GetAll();
        }

        public async Task<IEnumerable<TrainingResponse>> GetAllTrainingsForList()
        {
            return await _trainingRepository.GetAllTrainingsForList();
        }

        public async Task Update(Training training, Training entity)
        {
            await _trainingRepository.Update(training, entity);
        }
    }
}
