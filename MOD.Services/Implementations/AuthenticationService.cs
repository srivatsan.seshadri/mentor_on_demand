﻿using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Services.Interfaces;
using System.Threading.Tasks;

namespace MOD.Services.Implementations
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAuthenticationRepository _authenticationRepository;
        public AuthenticationService(IAuthenticationRepository authenticationRepository)
        {
            _authenticationRepository = authenticationRepository;
        }
        public async Task<User> ValidateUser(string userName, string password)
        {
            var user = await _authenticationRepository.ValidateUser(userName, password);
            return user;
        }

        public async Task UpdateLastLogin(string userName)
        {
            await _authenticationRepository.UpdateLastLogin(userName);
        }


    }
}
