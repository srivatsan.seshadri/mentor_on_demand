﻿using Microsoft.AspNetCore.Mvc;
using MOD.Models.Requests;
using MOD.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MOD.TechnologyService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TechnologyController : ControllerBase
    {
        private readonly ITechnologyService _technologyService;
        public TechnologyController(ITechnologyService technologyService)
        {
            _technologyService = technologyService;
        }

        [HttpGet]
        [Route("GetAllTechnologies")]
        public async Task<IActionResult> GetAllTechnologies()
        {
            try
            {
                var users = await _technologyService.GetAll();
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("GetTechnologyById/{id}")]
        public async Task<IActionResult> GetTechnologyById(int id)
        {
            try
            {
                var users = await _technologyService.Get(id);
                return Ok(users);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("AddTechnology")]
        public async Task<IActionResult> AddTechnology(TechnologyRequest request)
        {
            try
            {
                await _technologyService.Add(request.Technology);
                return Ok(request.Technology);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPut]
        [Route("UpdateTechnology")]
        public async Task<IActionResult> UpdateTechnology(TechnologyRequest request)
        {
            try
            {
                var user = await _technologyService.Get(request.Technology.Id);
                await _technologyService.Update(user, request.Technology);
                return Ok(request.Technology);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [Route("DeleteTechnology")]
        public async Task<IActionResult> DeleteTechnology(TechnologyRequest request)
        {
            try
            {
                await _technologyService.Delete(request.Technology);
                return Ok(request.Technology);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}