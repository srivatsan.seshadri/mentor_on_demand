﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Repository.Contexts
{
    public class ModContext : DbContext
    {
        public ModContext(DbContextOptions options)
               : base(options)
        {

        }

       // public DbSet<Mentor> Mentors { get; set; }
        public DbSet<MentorCalendar> MentorCalendars { get; set; }
        public DbSet<MentorSkill> MentorSkills { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Technology> Technologies { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Proposal> Proposals { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ///Mentor
            //modelBuilder.Entity<Mentor>().ToTable("Mentors");

            ///MentorCalendar
            modelBuilder.Entity<MentorCalendar>().HasKey(s => s.Id);
            modelBuilder.Entity<MentorCalendar>()
                        .HasOne(s => s.Mentor)
                        .WithMany(x => x.MentorCalendars)
                        .HasForeignKey(y => y.MentorId)
                        .HasConstraintName("ForeignKey_MentorCalendar_Mentor");

            ///MentorSkill
            modelBuilder.Entity<MentorSkill>().HasKey(s => s.Id);
            modelBuilder.Entity<MentorSkill>()
                        .HasOne(s => s.Mentor)
                        .WithMany(x => x.MentorSkills)
                        .HasForeignKey(y => y.MentorId)
                        .HasConstraintName("ForeignKey_MentorSkill_Mentor");

            ///Payment
            modelBuilder.Entity<Payment>().HasKey(s => s.Id);
            modelBuilder.Entity<Payment>()
                        .HasOne(s => s.Mentor)
                        .WithMany(x => x.MentorPayments)
                        .HasForeignKey(y => y.MentorId)
                        .HasConstraintName("ForeignKey_Payment_Mentor");
            modelBuilder.Entity<Payment>()
                        .HasOne(s => s.Training)
                        .WithOne(x => x.Payment)
                        .HasForeignKey("Training", new string[] { "TrainingId" })
                        .HasConstraintName("ForeignKey_Payment_Training");

            modelBuilder.Entity<Payment>()
            .HasOne<Training>(s => s.Training)
            .WithOne(ad => ad.Payment)
            .HasForeignKey<Training>(ad => ad.PaymentId)
            .HasConstraintName("ForeignKey_Payment_Training");

            ///Role
            modelBuilder.Entity<Role>().HasKey(s => s.RoleId);
            modelBuilder.Entity<Role>()
                        .HasMany(s => s.Users)
                        .WithOne(x => x.Role)
                        .HasForeignKey(y => y.RoleId)
                        .HasConstraintName("ForeignKey_User_Role");

            modelBuilder.Entity<Technology>().HasKey(s => s.Id);

            ///Training
            modelBuilder.Entity<Training>().HasKey(s => s.Id);

            modelBuilder.Entity<Training>()
                        .HasOne(s => s.Mentor)
                        .WithMany(ad => ad.MentorTrainings)
                        .HasForeignKey(ad => ad.MentorId)
                        .HasConstraintName("ForeignKey_Training_Mentor");

            modelBuilder.Entity<Training>()
                        .HasOne(s => s.Payment)
                        .WithOne(ad => ad.Training)
                        .HasForeignKey<Payment>(ad => ad.TrainingId)
                        .HasConstraintName("ForeignKey_Training_Payment");

            ///User
            modelBuilder.Entity<User>().HasKey(s => s.Id);
            modelBuilder.Entity<User>()
                        .HasOne(s => s.Role)
                        .WithMany(ad => ad.Users)
                        .HasForeignKey(ad => ad.RoleId)
                        .HasConstraintName("ForeignKey_User_Role");
        }
    }
}
