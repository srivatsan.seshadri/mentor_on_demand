﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Interfaces
{
    public interface ITrainingRepository
    {
        Task<IEnumerable<Training>> GetAll();
        Task<Training> Get(long id);
        Task Add(Training entity);
        Task Update(Training training, Training entity);
        Task Delete(Training training);
        Task<IEnumerable<TrainingResponse>> GetAllTrainingsForList();
    }
}
