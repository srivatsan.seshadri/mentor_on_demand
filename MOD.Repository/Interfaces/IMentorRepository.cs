﻿using MOD.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MOD.Repository.Interfaces
{
    public interface IMentorRepository
    {
        Task<IEnumerable<Mentor>> GetAll();
        Task<Mentor> Get(long id);
        Task Add(Mentor entity);
        Task Update(Mentor mentor, Mentor entity);
        Task Delete(Mentor user);
    }
}
