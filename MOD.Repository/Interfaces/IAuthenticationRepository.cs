﻿using MOD.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Interfaces
{
    public interface IAuthenticationRepository
    {
        Task<User> ValidateUser(string userName, string password);
        Task UpdateLastLogin(string userName);
    }
}
