﻿using MOD.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MOD.Repositories.Interfaces
{
    public interface IProposalRepository
    {
        Task<IEnumerable<Proposal>> GetAll();
        Task<Proposal> Get(long id);
        Task Add(Proposal entity);
        Task Update(Proposal payment, Proposal entity);
        Task Delete(Proposal payment);
    }
}
