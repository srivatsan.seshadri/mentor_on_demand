﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repository.Contexts;
using MOD.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOD.Repository.Implementations
{
    public class MentorRepository : IMentorRepository
    {
        private readonly ModContext _modContext;

        public MentorRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task Add(Mentor entity)
        {
            await _modContext.Mentors.AddAsync(entity);
            foreach (var mentorSkill in entity.MentorSkills)
            {
                await _modContext.MentorSkills.AddAsync(mentorSkill);
            }
            await _modContext.SaveChangesAsync();
        }

        public async Task Delete(Mentor user)
        {
            _modContext.Mentors.Remove(user);
            await _modContext.SaveChangesAsync();
        }

        public async Task<Mentor> Get(long id)
        {
            return await _modContext.Mentors
                  .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<IEnumerable<Mentor>> GetAll()
        {
            var mentors= await _modContext.Users
                                    .Join(_modContext.Roles, o => o.RoleId, i => i.RoleId, (o, i) => new { Mentors = o, Roles = i })
                                    .Where(s=>s.Roles.RoleName=="Mentor")
                                    .Select(s => new Mentor
                                    {
                                        Active = s.Mentors.Active,
                                        ContactNumber = s.Mentors.ContactNumber,
                                        FirstName = s.Mentors.FirstName,
                                        ForceResetPassword = s.Mentors.ForceResetPassword,
                                        Id = s.Mentors.Id,
                                        LastLoginDate = s.Mentors.LastLoginDate,
                                        LastName = s.Mentors.LastName,
                                        RegCode = s.Mentors.RegCode,
                                        RegDateTime = s.Mentors.RegDateTime,
                                        RoleId = s.Mentors.RoleId,
                                        UserName = s.Mentors.UserName,
                                        Role = new Role
                                        {
                                            RoleId = s.Roles.RoleId,
                                            RoleName = s.Roles.RoleName,
                                            RoleDesc = s.Roles.RoleDesc
                                        }
                                    })
                                    .ToListAsync();
            foreach (var mentor in mentors)
            {
                mentor.MentorSkills = await _modContext.MentorSkills.Where(s => s.MentorId == mentor.Id).ToListAsync();
            }
            return mentors;
        }

        public async Task Update(Mentor mentor, Mentor entity)
        {
            mentor.Active = entity.Active;
            mentor.ContactNumber = entity.ContactNumber;
            mentor.FirstName = entity.FirstName;
            mentor.ForceResetPassword = entity.ForceResetPassword;
            mentor.LastLoginDate = entity.LastLoginDate;
            mentor.LastName = entity.LastName;
            mentor.Password = entity.Password;
            mentor.RegCode = entity.RegCode;
            mentor.RegDateTime = entity.RegDateTime;
            mentor.RoleId = entity.RoleId;
            mentor.UserName = entity.UserName;

            mentor.MentorSkills.Clear();
            foreach (var mentorSkill in mentor.MentorSkills)
            {
                await _modContext.MentorSkills.AddAsync(mentorSkill);
            }
            await _modContext.SaveChangesAsync();
        }
    }
}
