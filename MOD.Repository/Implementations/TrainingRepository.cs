﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class TrainingRepository : ITrainingRepository
    {
        private readonly ModContext _modContext;

        public TrainingRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task<IEnumerable<Training>> GetAll()
        {
            try
            {
                return await _modContext.Trainings
                                  .Join(_modContext.Users, o => o.MentorId, i => i.Id, (o, i) => new { Training = o, User = i })
                                  .Join(_modContext.Technologies, o => o.Training.SkillId, i => i.Id, (o, i) => new { Outer = o, Technology = i })
                                  .Select(s => new Training
                                  {
                                      Id = s.Outer.Training.Id,
                                      AmountReceived = s.Outer.Training.AmountReceived,
                                      EndDate = s.Outer.Training.EndDate,
                                      Mentor = s.Outer.User,
                                      MentorId = s.Outer.Training.MentorId,
                                      Payment = s.Outer.Training.Payment,
                                      PaymentId = s.Outer.Training.PaymentId,
                                      Progress = s.Outer.Training.Progress,
                                      Rating = s.Outer.Training.Rating,
                                      SkillId = s.Outer.Training.SkillId,
                                      Skill = s.Technology,
                                      StartDate = s.Outer.Training.StartDate,
                                      Status = s.Outer.Training.Status,
                                      UserId = s.Outer.Training.UserId,
                                      Fees = s.Outer.Training.Fees
                                  })
                                  .ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Training> Get(long id)
        {
            return await _modContext.Trainings
                  .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Add(Training entity)
        {
            await _modContext.Trainings.AddAsync(entity);
            await _modContext.SaveChangesAsync();

            await _modContext.MentorCalendars.AddAsync(new MentorCalendar
            {
                EndDate = entity.EndDate,
                Mentor = entity.Mentor,
                MentorId = entity.MentorId,
                StartDate = entity.StartDate,
                TrainingId = entity.Id
            });
            await _modContext.SaveChangesAsync();

        }

        public async Task Update(Training training, Training entity)
        {
            training.AmountReceived = entity.AmountReceived;
            training.EndDate = entity.EndDate;
            training.MentorId = entity.MentorId;
            training.PaymentId = entity.PaymentId;
            training.Progress = entity.Progress;
            training.Rating = entity.Rating;
            training.SkillId = entity.SkillId;
            training.StartDate = entity.StartDate;
            training.Status = entity.Status;
            training.UserId = entity.UserId;
            training.Fees = entity.Fees;
            await _modContext.SaveChangesAsync();

            var mentorCalendar = await _modContext.MentorCalendars.FirstOrDefaultAsync(s => s.TrainingId == entity.Id);
            mentorCalendar.EndDate = entity.EndDate;
            mentorCalendar.Mentor = entity.Mentor;
            mentorCalendar.MentorId = entity.MentorId;
            mentorCalendar.StartDate = entity.StartDate;
            await _modContext.SaveChangesAsync();

        }

        public async Task Delete(Training training)
        {
            _modContext.Trainings.Remove(training);
            await _modContext.SaveChangesAsync();
            var mentorCalendar = await _modContext.MentorCalendars.FirstOrDefaultAsync(s => s.TrainingId == training.Id);
            _modContext.MentorCalendars.Remove(mentorCalendar);
            await _modContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<TrainingResponse>> GetAllTrainingsForList()
        {
            try
            {
                return await _modContext.Trainings
                                  .Join(_modContext.Users, o => o.MentorId, i => i.Id, (o, i) => new { Training = o, User = i })
                                  .Join(_modContext.Technologies, o => o.Training.SkillId, i => i.Id, (o, i) => new { Outer = o, Technology = i })
                                  .Select(s => new TrainingResponse
                                  {
                                      Id = s.Outer.Training.Id,
                                      AmountReceived = s.Outer.Training.AmountReceived,
                                      EndDate = s.Outer.Training.EndDate,
                                      MentorName = $"{s.Outer.User.FirstName} {s.Outer.User.LastName}",
                                      MentorId = s.Outer.Training.MentorId,
                                      Progress = s.Outer.Training.Progress,
                                      Rating = s.Outer.Training.Rating,
                                      SkillId = s.Outer.Training.SkillId,
                                      Skill = s.Technology.Name,
                                      StartDate = s.Outer.Training.StartDate,
                                      Status = s.Outer.Training.Status,
                                      Fees = s.Outer.Training.Fees
                                  })
                                  .ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
