﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class TechnologyRepository: ITechnologyRepository
    {
        private readonly ModContext _modContext;

        public TechnologyRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task<IEnumerable<Technology>> GetAll()
        {
            return await _modContext.Technologies.ToListAsync();
        }

        public async Task<Technology> Get(long id)
        {
            return await _modContext.Technologies
                  .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Add(Technology entity)
        {
            await _modContext.Technologies.AddAsync(entity);
            await _modContext.SaveChangesAsync();
        }

        public async Task Update(Technology technology, Technology entity)
        {
            technology.Duration = entity.Duration;
            technology.Name = entity.Name;
            technology.Prerequisites = entity.Prerequisites;
            technology.TermsOfConditions = entity.TermsOfConditions;
            await _modContext.SaveChangesAsync();
        }

        public async Task Delete(Technology technology)
        {
            _modContext.Technologies.Remove(technology);
            await _modContext.SaveChangesAsync();
        }
    }
}
