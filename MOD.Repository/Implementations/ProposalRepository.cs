﻿using MOD.Models.Models;
using MOD.Repository.Contexts;
using MOD.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MOD.Repositories.Implementations
{
    public class ProposalRepository : IProposalRepository
    {
        private readonly ModContext _modContext;

        public ProposalRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task Add(Proposal entity)
        {
            await _modContext.Proposals.AddAsync(entity);
            await _modContext.SaveChangesAsync();
        }

        public async Task Delete(Proposal proposal)
        {
            proposal.ProposalDate = DateTime.Now;
            _modContext.Proposals.Remove(proposal);
            await _modContext.SaveChangesAsync();
        }

        public async Task<Proposal> Get(long id)
        {
            var proposal = await _modContext.Proposals
                  .FirstOrDefaultAsync(e => e.Id == id);
            return proposal;
        }

        public async Task<IEnumerable<Proposal>> GetAll()
        {
            return await _modContext.Proposals.ToListAsync();
        }

        public async Task Update(Proposal proposal, Proposal entity)
        {
            proposal.Email = entity.Email;
            proposal.FirstName = entity.FirstName;
            proposal.LastName = entity.LastName;
            proposal.MentorId = entity.MentorId;
            proposal.Mobile = entity.Mobile;
            await _modContext.SaveChangesAsync();
        }
    }
}
