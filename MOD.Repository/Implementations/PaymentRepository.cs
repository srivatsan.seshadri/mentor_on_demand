﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly ModContext _modContext;

        public PaymentRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task<IEnumerable<Payment>> GetAll()
        {
            var payments = await _modContext.Payments
                                   .Join(_modContext.Trainings, o => o.TrainingId, i => i.Id, (o, i) => new { Payment = o, Training = i })
                                   .Join(_modContext.Users, o => o.Payment.MentorId, i => i.Id, (o, i) => new { PaymentDetails = o, Mentor = i })
                                   .Select(s => new Payment
                                   {
                                       Id = s.PaymentDetails.Payment.Id,
                                       Amount = s.PaymentDetails.Payment.Amount,
                                       MentorId = s.PaymentDetails.Payment.MentorId,
                                       Mentor = s.Mentor,
                                       PaymentDate = s.PaymentDetails.Payment.PaymentDate,
                                       Remarks = s.PaymentDetails.Payment.Remarks,
                                       TrainingId = s.PaymentDetails.Payment.TrainingId,
                                       TxnType = s.PaymentDetails.Payment.TxnType
                                   })
                                   .ToListAsync();
            return payments;
        }

        public async Task<Payment> Get(long id)
        {
            return await _modContext.Payments
                  .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Add(Payment entity)
        {
            await _modContext.Payments.AddAsync(entity);
            await _modContext.SaveChangesAsync();
            var training = await _modContext.Trainings.FirstOrDefaultAsync(s => s.Id == entity.TrainingId);
            training.AmountReceived = training.AmountReceived + entity.Amount;
            await _modContext.SaveChangesAsync();
        }

        public async Task Update(Payment payment, Payment entity)
        {
            payment.Amount = entity.Amount;
            payment.MentorId = entity.MentorId;
            payment.PaymentDate = entity.PaymentDate;
            payment.Remarks = entity.Remarks;
            payment.TrainingId = entity.TrainingId;
            payment.TxnType = entity.TxnType;
            await _modContext.SaveChangesAsync();
            await _modContext.SaveChangesAsync();
        }

        public async Task Delete(Payment payment)
        {
            _modContext.Payments.Remove(payment);
            await _modContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<PaymentResponse>> GetAllPaymentsForList()
        {
            var payments = await _modContext.Payments
                                  .Join(_modContext.Trainings, o => o.TrainingId, i => i.Id, (o, i) => new { Payment = o, Training = i })
                                  .Join(_modContext.Users, o => o.Payment.MentorId, i => i.Id, (o, i) => new { PaymentDetails = o, Mentor = i })
                                  .Join(_modContext.Technologies, o => o.PaymentDetails.Training.SkillId, i => i.Id, (o, i) => new { Outer = o, Skill = i })
                                  .Select(s => new PaymentResponse
                                  {
                                      Id = s.Outer.PaymentDetails.Payment.Id,
                                      Amount = s.Outer.PaymentDetails.Payment.Amount,
                                      MentorId = s.Outer.PaymentDetails.Payment.MentorId,
                                      MentorFirstName = s.Outer.Mentor.FirstName,
                                      MentorLastName = s.Outer.Mentor.LastName,
                                      Training = $"{s.Outer.Mentor.FirstName} {s.Outer.Mentor.LastName} {s.Skill.Name} {s.Outer.PaymentDetails.Training.StartDate} {s.Outer.PaymentDetails.Training.EndDate}",
                                      PaymentDate = s.Outer.PaymentDetails.Payment.PaymentDate,
                                      Remarks = s.Outer.PaymentDetails.Payment.Remarks,
                                      TrainingId = s.Outer.PaymentDetails.Payment.TrainingId,
                                      TxnType = s.Outer.PaymentDetails.Payment.TxnType
                                  })
                                  .ToListAsync();
            return payments;
        }
    }
}
