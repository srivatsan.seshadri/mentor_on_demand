﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly ModContext _modContext;
        public AuthenticationRepository(ModContext ctx)
        {
            _modContext = ctx;
        }
        public async Task UpdateLastLogin(string userName)
        {
            var user = await _modContext.Users.FirstOrDefaultAsync(s => s.UserName == userName);
            user.LastLoginDate = DateTime.Now;
            _modContext.Update(user);
            await _modContext.SaveChangesAsync();
        }

        public async Task<User> ValidateUser(string userName, string password)
        {
            var user = await _modContext.Users.FirstOrDefaultAsync(s => s.UserName == userName && s.Password == password && s.Active);
            return user;
        }
    }
}
