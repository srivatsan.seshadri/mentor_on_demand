﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly ModContext _modContext;

        public UserRepository(ModContext context)
        {
            _modContext = context;
        }
        public async Task<IEnumerable<User>> GetAll()
        {
            return await _modContext.Users
                                    .Join(_modContext.Roles, o => o.RoleId, i => i.RoleId, (o, i) => new { Users = o, Roles = i })
                                    .Select(s => new User
                                    {
                                        Active = s.Users.Active,
                                        ContactNumber = s.Users.ContactNumber,
                                        FirstName = s.Users.FirstName,
                                        ForceResetPassword = s.Users.ForceResetPassword,
                                        Id = s.Users.Id,
                                        LastLoginDate = s.Users.LastLoginDate,
                                        LastName = s.Users.LastName,
                                        RegCode = s.Users.RegCode,
                                        RegDateTime = s.Users.RegDateTime,
                                        RoleId = s.Users.RoleId,
                                        UserName = s.Users.UserName,
                                        Role = new Role
                                        {
                                            RoleId = s.Roles.RoleId,
                                            RoleName = s.Roles.RoleName,
                                            RoleDesc = s.Roles.RoleDesc
                                        }
                                    })
                                    .ToListAsync();
        }

        public async Task<User> Get(long id)
        {
            var userData = await _modContext.Users
                  .FirstOrDefaultAsync(e => e.Id == id);
            return userData;
        }

        public async Task Add(User entity)
        {
            await _modContext.Users.AddAsync(entity);
            await _modContext.SaveChangesAsync();
            foreach (var skill in entity.MentorSkills)
            {
                skill.MentorId = entity.Id;
                await _modContext.MentorSkills.AddAsync(skill);
            }
            //await _modContext.SaveChangesAsync();
        }

        public async Task Update(User user, User entity)
        {
            user.Active = entity.Active;
            user.ContactNumber = entity.ContactNumber;
            user.FirstName = entity.FirstName;
            user.ForceResetPassword = entity.ForceResetPassword;
            user.LastLoginDate = entity.LastLoginDate;
            user.LastName = entity.LastName;
            user.Password = entity.Password;
            user.RegCode = entity.RegCode;
            user.RegDateTime = entity.RegDateTime;
            user.RoleId = entity.RoleId;
            user.UserName = entity.UserName;
            user.LinkedInUrl = entity.LinkedInUrl;
            user.YearsOfExperience = entity.YearsOfExperience;
            await _modContext.SaveChangesAsync();

            var userSkills = _modContext.MentorSkills.Where(s => s.MentorId == entity.Id);
            if (userSkills != null && userSkills.Count() > 0)
            {
                foreach (var userSkill in userSkills)
                {
                    _modContext.MentorSkills.Remove(userSkill);
                }
                await _modContext.SaveChangesAsync();
            }

            foreach (var skill in entity.MentorSkills)
            {
                skill.Id = 0;
                skill.MentorId = entity.Id;
                await _modContext.MentorSkills.AddAsync(skill);
            }
            await _modContext.SaveChangesAsync();
        }

        public async Task Delete(User user)
        {
            _modContext.Users.Remove(user);
            await _modContext.SaveChangesAsync();
            var userSkills = _modContext.MentorSkills.Where(s => s.MentorId == user.Id);
            if (userSkills != null && userSkills.Count() > 0)
            {
                foreach (var userSkill in userSkills)
                {
                    _modContext.MentorSkills.Remove(userSkill);
                }
                await _modContext.SaveChangesAsync();
            }
        }

        public async Task<List<Role>> GetAllRoles()
        {
            return await _modContext.Roles.ToListAsync();
        }

        public async Task<List<MentorSkill>> GetUserMentorSkills(long userId)
        {
            var userSkills = await _modContext.MentorSkills
                                              .Where(u => u.MentorId == userId)
                                              .ToListAsync();
            return userSkills;
        }

        public async Task<List<User>> GetAllMentors()
        {
            var role = await _modContext.Roles.FirstOrDefaultAsync(r => r.RoleName == "Mentor");
            var users = await _modContext.Users
                                              .Where(u => u.RoleId == role.RoleId)
                                              .Select(s=>new User {
                                                  Active=s.Active,
                                                  ContactNumber=s.ContactNumber,
                                                  FirstName=s.FirstName,
                                                  ForceResetPassword=s.ForceResetPassword,
                                                  RoleId=s.RoleId,
                                                  Id=s.Id,
                                                  LastLoginDate=s.LastLoginDate,
                                                  LastName=s.LastName,
                                                  LinkedInUrl=s.LinkedInUrl,
                                                  RegCode=s.RegCode,
                                                  RegDateTime=s.RegDateTime,
                                                  UserName=s.UserName,
                                                  YearsOfExperience=s.YearsOfExperience
                                              })
                                              .ToListAsync();
            return users;
        }
    }
}
