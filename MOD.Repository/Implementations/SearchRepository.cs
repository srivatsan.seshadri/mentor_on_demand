﻿using Microsoft.EntityFrameworkCore;
using MOD.Models.Models;
using MOD.Repositories.Interfaces;
using MOD.Repository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOD.Repositories.Implementations
{
    public class SearchRepository : ISearchRepository
    {
        private readonly ModContext _modContext;

        public SearchRepository(ModContext context)
        {
            _modContext = context;
        }

        public async Task<IEnumerable<SearchResult>> SearchMentorsForTechnologies(int technology, DateTime? fromDate, DateTime? toDate)
        {
            var payments = await _modContext.Technologies.Where(x => x.Id == technology)
                                   .Join(_modContext.MentorSkills, o => o.Id, i => i.SkillId, (o, i) => new { Technology = o, MentorSkill = i })
                                   .Join(_modContext.Users, o => o.MentorSkill.MentorId, i => i.Id, (o, i) => new { MentorDetail = o, User = i })
                                   .Join(_modContext.MentorCalendars, o => o.User.Id, i => i.MentorId, (o, i) => new { MentorTrainingDetail = o, MentorCalendar = i })
                                   .Where(s => !((s.MentorCalendar.StartDate == fromDate) && (s.MentorCalendar.EndDate == toDate)))
                                   .Select(s => new SearchResult
                                   {
                                       Id = s.MentorTrainingDetail.User.Id,
                                       FirstName = s.MentorTrainingDetail.User.FirstName,
                                       LastName = s.MentorTrainingDetail.User.LastName,
                                       Rating = s.MentorTrainingDetail.MentorDetail.MentorSkill.SelfRating,
                                       YearsOfExperience = s.MentorTrainingDetail.User.YearsOfExperience.Value
                                   })
                                   .ToListAsync();
            return payments;
        }
    }
}
